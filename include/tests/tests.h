#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

#include "../../include/mem/mem_internals.h"

void attempt1(struct block_header *first_block);

void attempt2(struct block_header *first_block);

void attempt3(struct block_header *first_block);

void attempt4(struct block_header *first_block);

void attempt5(struct block_header *first_block);

void start_tests();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
