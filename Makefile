CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Werror -Wextra -DDEBUG
BUILDDIR=build
SRCDIR=src
CC=gcc
mode=debug

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/tests.o $(BUILDDIR)/main.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)


$(BUILDDIR)/mem.o: $(SRCDIR)/mem/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/tests.o: $(SRCDIR)/tests/tests.c build
	$(CC) -c $(CFLAGS) $< -o $@


$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@


clean:
	rm -rf $(BUILDDIR)

run:
	./$(BUILDDIR)/main


