#define _DEFAULT_SOURCE

#include "../../include/tests/tests.h"


#include "../../include/mem/mem.h"
#include "../../include/util/util.h"


void debug(const char *fmt, ...);

struct block_header *get_block_header(void *data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

void attempt1(struct block_header *first_block) {
    debug("Test 1:  Single selection and cleaning\n");
    void *mem = _malloc(2021);
    if (mem == NULL)
        err("Bullshit: mem is NULL");

    debug_heap(stdout, first_block);

    if (first_block->is_free != false || first_block->capacity.bytes != 2021)
        err("Bullshit: incorrect block size or it's free one");

    _free(mem);
    debug("Test 1 is completed.\n\n");
}

void attempt2(struct block_header *first_block) {
    debug("Test 2: two allocations and one free\n");
    void *mem1 = _malloc(666);
    void *mem2 = _malloc(1650);
    if (mem1 == NULL || mem2 == NULL)
        err("Bullshit: memX is NULL");

    _free(mem1);
    debug_heap(stdout, first_block);
    struct block_header *mem1_block = get_block_header(mem1);
    struct block_header *mem2_block = get_block_header(mem2);

    if (mem1_block->is_free == false)
        err("Bullshit: first block isn't free");

    if (mem2_block->is_free == true)
        err("Bullshit: second block isn't free");

    _free(mem2);
    debug("Test 2 is completed.\n\n");
}

void attempt3(struct block_header *first_block) {
    debug("Test 3: cleaning two blocks of three, central one will be cleaned first\n");
    void *mem1 = _malloc(4000);
    void *mem2 = _malloc(3000);
    void *mem3 = _malloc(3000);
    if (mem1 == NULL || mem2 == NULL || mem3 == NULL)
        err("Bullshit: memX is NULL");

    _free(mem2);
    _free(mem1);
    debug_heap(stdout, first_block);

    struct block_header *mem1_header = get_block_header(mem1);
    struct block_header *mem2_header = get_block_header(mem2);
    struct block_header *mem3_header = get_block_header(mem3);

    if (mem1_header->is_free == false)
        err("Bullshit: free first block isn't free 0_o");
    if (mem2_header->is_free == false)
        err("Bullshit: free second block isn't free 0_o");
    if (mem3_header->is_free == true)
        err("Bullshit: free third block isn't free 0_o");

    if (mem1_header->capacity.bytes != 7000 + offsetof(struct block_header, contents))
        err("Incorrect block size");

    _free(mem3);
    debug("Test 3 is completed.\n\n");
}

void attempt4(struct block_header *first_block) {
    debug("Test 4: launching minecraft (big region)\n");
    void *mem1 = _malloc(32000);
    void *mem2 = _malloc(32000);
    void *mem3 = _malloc(32000);
    if (mem1 == NULL || mem2 == NULL || mem3 == NULL)
        err("Bullshit: memX is NULL");

    debug_heap(stdout, first_block);
    struct block_header *mem1_header = get_block_header(mem1);
    struct block_header *mem2_header = get_block_header(mem2);

    if ((uint8_t *) mem1_header->contents + mem1_header->capacity.bytes != (uint8_t *) mem2_header)
        err("Bullshit: Block was allocated at the wrong place");

    _free(mem3);
    _free(mem2);
    _free(mem1);

    debug("Test 4 is completed.\n\n");
}

void attempt5(struct block_header *first_block) {
    debug("Тест 5: the end...\n");

    void *mem1 = _malloc(10000);
    if (mem1 == NULL)
        err("Bullshit: mem1 is NULL");

    struct block_header *mem1_block = first_block;
    while (mem1_block->next != NULL)
        mem1_block = mem1_block->next;

    map_pages((uint8_t *) mem1_block + size_from_capacity(mem1_block->capacity).bytes, 1000, MAP_FIXED_NOREPLACE);
    void *mem2 = _malloc(300000);

    debug_heap(stdout, first_block);
    struct block_header *mem2_block = get_block_header(mem2);
    if (mem2_block == mem1_block)
        err("Bullshit: Second block was allocated near the first one");

    _free(mem1);
    _free(mem2);
    debug("Test 5 is completed.\n\n");
}


void start_tests() {

    void *heap = heap_init(10000);
    if (heap == NULL)
        err("We got some bullshit");

    debug("Testing:\n\n");

    attempt1(heap);
    attempt2(heap);
    attempt3(heap);
    attempt4(heap);
    attempt5(heap);
    debug("All tests are passed\n");
}


